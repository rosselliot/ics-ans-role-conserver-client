import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_conserver_client(host):
    assert host.exists('console')
    assert host.run_test('console -u')
    consoles = host.run("console -u")
    assert "noop" in consoles.stdout
